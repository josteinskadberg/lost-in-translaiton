# trivia-game
heroku-url: https://frozen-ravine-12161.herokuapp.com/

## Contributors:
Jostein Skadberg - gitlab.com/josteinskadberg 

Amalie Espeseth - gitlab.com/amalie.e

## Description:
An application for translating english letters to sign language letters. 

### Login Page:

Will ask you for a username to log in with. You may also create a new user. Your username will be stored in the context API and the session storage in the browser. When you click the log in button it will take you to the translation page. 

### Translation page:

Write up to 40 letters to translate into corresponding sign language pictures. Your translations will be stored in the API and the session storage. If you are not logged in you will not have access to this page. You may also click on the profile button in the nav bar which will take you to the profile page.

### Profile page:

Will show your 10 most recent translations in regular letters. If you press the delete button you delete your stored translations, and the previously translated letters will appear, if there are any. If you have deleted all your translations none will appear.
The log off button will take you to the log in page. You can not access the profile page unless you are logged in. If you log off your username and translations will be stored in the session storage and will be available again when you log back in. If you terminate the process, the username and translations are stored in the translate API.

## Instructions

If you want to run project locally follow three steps: 

**You need to have npm installed**

1. clone this repo
2. `npm install` command in the root folder
3. `npm start` command in the root folder 




