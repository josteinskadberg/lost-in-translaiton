import './App.css';
import  {BrowserRouter, Route, Routes} from 'react-router-dom'
import Login from './views/Login';
import Translation from './views/Translation';
import Profile from './views/Profile';


function App() {
  return (
  
      <BrowserRouter>  
      <div className="App">
      <Routes>
        <Route path = "/"  element = {<Login/>}/>
        <Route path = "/translate" element = {<Translation/>}/>
        <Route path = "/profile" element = {<Profile/>}/>
      </Routes>
      </div>
      </BrowserRouter>
  );
}

export default App;
