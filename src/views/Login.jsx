import LoginForm from "../components/Login/LoginForms"
import "../components/Login/LoginForms.css"
import Navbar from "../components/Navbar/Navbar"
const Login = () => {
    return (
        <>
            <Navbar to="/profile" />
            <header>
                <h2>Lost in translation</h2>
                <h3>Let's get startet</h3>
                <img className="homeLogo" alt="" src="Logo-Hello.png" />
            </header>
            <LoginForm />
        </>
    )
}

export default Login