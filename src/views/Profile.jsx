import Navbar from "../components/Navbar/Navbar"
import { useUser } from "../contex/UserContex"
import { useEffect } from "react"
import { storageRead } from "../utils/storage"
import { useNavigate } from "react-router-dom"
import ProfileDetails from "../components/Profile/ProfileDetails"
import "../components/Profile/ProfileDetails.css"

const Profile = () => {

    return (
        <>
            <Navbar to='/translate' />
            <div className="header">
                <h1>Profile</h1>
            </div>
            <ProfileDetails></ProfileDetails>
        </>
    )
}

export default Profile