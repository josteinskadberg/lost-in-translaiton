import Input from "../components/Translation/Input"
import Navbar from "../components/Navbar/Navbar"
import TranslationOutput from "../components/Translation/TranslationOutput"
import { useEffect, useState } from "react"
import { useUser } from "../contex/UserContex"
import { storageRead, storageSave } from "../utils/storage"
import { useNavigate } from 'react-router-dom'
import uniqid from 'uniqid'
import { patchTranslationList } from "../api/user"


const Translation = () => {
    
    //HOOKS
    const { user, setUser } = useUser()
    const navigate = useNavigate()

    //LOCAL STORAGE
    const [currentTranslation, setCurrentTranslation] = useState("")

    //SIDE EFFECTS 
    
    //Redirect to login page if not loged inn 
    useEffect(() => {
        if (!user) {
            const sessionUser = storageRead("user")
            if (sessionUser) {
                setUser(sessionUser)
            }
            else (
                navigate("/")
            )
        }
    }, [user])

    //EVENT HANDLERS 

    /*Create translation object save in Context, Session Storage
         and Update translation history in tranlateAPI*/
    const onSubmit = async (text) => {
        setCurrentTranslation(text.translationInput)
        if (text !== "") {
            user.translations.push({ text: text.translationInput, isDeleted: false, id: uniqid() })
            setUser(user)
            storageSave("user", user)
            await patchTranslationList(user.id, user.translations)
        }

    }

    return (
        <>
            <Navbar to="/profile" />
            <Input onSubmit={onSubmit} />
            <TranslationOutput value={currentTranslation} />
        </>
    )
}
export default Translation