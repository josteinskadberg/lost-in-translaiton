import { createHeaders } from "./index"

const apiURL = process.env.REACT_APP_API_URL


//GET user details for given username
const checkForUser = async (username) =>{
    try{
        const response = await fetch(`${apiURL}?username=${username}`)
        if(!response.ok){ 
            throw new Error("something went wrong")
        }
        const data = await response.json()
        return [null, data]
    }
    catch(error){
        return[error.message, []]
    }
}

//Replace translation history in db with current translations history
export const patchTranslationList = async (userID,translations) => {
    try{ const response = await fetch(`${apiURL}/${userID}`,{
        method : "PATCH",
        headers: createHeaders(), 
        body: JSON.stringify({
            translations : translations
        })
    })
    if(!response.ok){
        throw new Error("somthing went wrong posting translation history")
    }
    const data = await response.json()
    return [null, data]
}
catch(error){
    return[error.message, []]
}
}

//POST new user
const createUser = async (username) =>{
    try{
        const response = await fetch(`${apiURL}`,{
            method : "POST",
            headers: createHeaders(), 
            body: JSON.stringify({
                username,
                translations : []
            })
        })
        if(!response.ok){
            throw new Error("could not create user with username" + username)
        }
        const data = await response.json()
        return [null, data]
    }
    catch(error){
        return[error.message, []]
    }
}

//Get user details for given username, register new user if usename not found 
export const loginUser = async (username) => {
    const [getError, user] = await checkForUser(username)
    if (getError !== null){
        return [getError, null]
    }
    if (user.length > 0){
        return [null, user.pop()]
    }
    return await createUser(username)
}
