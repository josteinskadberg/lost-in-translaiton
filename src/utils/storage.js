
//save to session storage
export const storageSave = (key,  value) => {
    sessionStorage.setItem(key, JSON.stringify(value))
}

//read from session storage
export const storageRead = key => {
    const data = sessionStorage.getItem(key)
    if (data){
        return JSON.parse(data)
    }
    return null
}