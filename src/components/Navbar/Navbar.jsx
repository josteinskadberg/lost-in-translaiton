import { CgProfile, CgClose } from "react-icons/cg"
import { useUser } from "../../contex/UserContex"
import { NavLink } from "react-router-dom"
import "./Navbar.css"

/* 
  Navigation bar for entire application 
  - Navigatates between translate page and profile page
  - Changes icon based on to from props 
 */

const Navbar = (props) => {
    //hooks 
    const { user, setUser } = useUser()

    //render username
    const username = () => {
        if (user) {
            return user.username
        }
        else return null
    }

    return (
        <div className="navbar" to="">
            <h3>Lost in translation </h3>
            <NavLink className="link" to={props.to}>
                {props.to !== "/profile" ? <p> <CgClose fontSize={"x-large"} /> </p> :
                    <p>{username()}  <CgProfile fontSize={"x-large"} /></p>}
            </NavLink>
        </div>
    )
}

export default Navbar