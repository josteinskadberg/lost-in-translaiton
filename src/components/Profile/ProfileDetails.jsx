import { useEffect, useState } from "react"
import { useUser } from "../../contex/UserContex"
import { patchTranslationList } from "../../api/user"
import { storageRead, storageSave } from "../../utils/storage"
import { useNavigate } from "react-router-dom"
import "./ProfileDetails.css"

/*
Profile Details displays the 10 most recent translation from translation history 
    - User can delete translation if more than 10 exist they are displayed on deletion 
    - User can log out from page resetting state in ContextAPI and Session Storage 
*/

const ProfileDetails = () => {

    //HOOKS 
    const navigate = useNavigate()
    const { user, setUser } = useUser()

    //LOCAL STORAGE
    const [displayedTranslations, setDisplayedTranslations] = useState([])

    //SIDE EFFECTS 

    /*redirect to login page if no user exist in Context or Session Storage. 
    Else display translation history.*/
    useEffect(() => {
        if (!user) {
            const sessionUser = storageRead("user")
            if (sessionUser) {
                setUser(sessionUser)
            }
            else (
                navigate("/")
            )
        }
        else {
            populateRecentTranslations()
        }
    }, [user])

  

   //EVENT HANDLERS 
    
   /* Delete displayed translations, render existing undelted translation 
       Save new translation history to Context state, session Storage and Translation API*/
    const onDelete = async () => {
        for (let displayed of displayedTranslations) {
            for (let translation of user.translations)
                if (displayed.id === translation.id) {
                    translation.isDeleted = true
                }
        }
        setDisplayedTranslations([])
        setUser(user)
        storageSave("user", user)
        populateRecentTranslations()
        await patchTranslationList(user.id, user.translations)

    }
    
    //reset state. Side Effect will rederect to login page
    const onLogout = () => {
        storageSave('user', null)
        setUser(null)

    }



    //UTILITY FUNCTIONS

      //Filter undeleted translations 
      const populateRecentTranslations = () => {
        const notDeleted = user.translations.filter(x => x.isDeleted === false)
        setDisplayedTranslations(notDeleted.slice(-10).reverse())
    }

    const username = () => {
        if (user) {
            return user.username
        }
        else return null
    }

 

    return (
        <>

            <div className="profileDetailContainer">
                {username()}
                <ul className="translastionHistory">
                    {displayedTranslations.map((translation, index) => {
                        return <li className="translastionRecord" key={translation.id}>{translation.text}</li>
                    })}
                </ul>
                <div className="buttonContainer">
                    <button className="profileButton" onClick={onDelete}>Delete Translation</button>
                    <button className="profileButton" onClick={onLogout}>Log Out</button>
                </div>
            </div>

        </>

    )
}

export default ProfileDetails