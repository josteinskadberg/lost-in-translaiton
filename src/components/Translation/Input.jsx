import "./translation.css"
import { useForm } from "react-hook-form"
import { useUser } from "../../contex/UserContex"

//Config for translation input
const translationConfig =  {
    required : true, 
    maxLength : 40,
}

const Input = (props) => {

    //HOOKS
    const {register, handleSubmit,formState: {errors}}  = useForm()

    //Render functions 

    //error messages for translate translation input 
    const errorMessage = (() => {
        if (!errors.translationInput){
            return null 
        }
        if (errors.translationInput.type === "maxLength"){
            return <span>Max 40 characters per translation</span> 
        }
        if (errors.translationInput.type === "required"){
            return <span>Please write something</span> 
        } 
      
        })(); 
 
    
    return ( 
        <form onSubmit={handleSubmit(props.onSubmit)}>
            <fieldset>
            <input className = "translationInput" placeholder= "Please write something" type= "text" {...register("translationInput",translationConfig)}/>
            <button className= "translateButton" type = "submit"> Translate</button>
            {errorMessage}
            </fieldset>
       
        </form>
    )
}

export default Input