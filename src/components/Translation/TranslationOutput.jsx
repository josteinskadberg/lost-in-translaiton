/*
Displays a set of chars as sign language pictures. 
*/
import "./translation.css"
const TranslationOutput = (props) => {
 
//Translate english letters to sign language
const translate = () => {
    if (props.value !== ""){
    const letters = props.value.split("")
    return letters.map(function(letter, index){
        letter = letter.toLowerCase()
        if (letter.match(/[a-z]/)) {
            return(<img className="handSigns" src = {`/individial_signs/${letter}.png`} key={ index } alt={ letter }/>)
        } else {
            return letter;
        }
    })}
    else{
        return null
    }
}


return (
    <div className= "translate">
        <ul id= "translationImages">
        { translate() }
        </ul>
    </div>
)
}

export default TranslationOutput
