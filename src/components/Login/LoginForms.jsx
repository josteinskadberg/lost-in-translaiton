import { useState, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { loginUser } from '../../api/user'
import { storageSave } from '../../utils/storage'
import { useNavigate } from 'react-router-dom'
import { useUser } from '../../contex/UserContex'
import './LoginForms.css'

//Config for username on login
const usernameConfig = {
    required: true,
    minLength: 3
}

/*
Logon Form component 
- handles registation and fecthing user information 
- saves current user to Session Storage and ContextAPI
*/

const LoginForm = () => {
    //Hooks 
    const { user, setUser } = useUser()
    const { register, handleSubmit, formState: { errors } } = useForm()
    const navigate = useNavigate()

    //Local State
    const [loading, setLoading] = useState(false)
    const [apiError, setApiError] = useState(null)


    //Side Effects 

    //navigate to translate page if user successully logged inn
    useEffect(() => {
        if (user !== null) {
            navigate("translate")
        }
    }, [user])


    //Event Handlers

    //Send login request and try to set user in state and session
    const onSubmit = async ({ username }) => {
        setLoading(true)
        const [error, userResponse] = await loginUser(username)
        console.log(error, userResponse)
        if (error) {
            setApiError(error)
        }
        setLoading(false)

        if (userResponse) {
            storageSave("user", userResponse)
            setUser(userResponse)

        }

    }

    //Render Functions
    
    //Errormessages for invalid username
    const errorMessage = (() => {
        if (!errors.username) {
            return null
        }
        switch (errors.username.type) {
            case ('required'): {
                return <span> You have to provide a username before you continue </span>
            }
            case ("minLength"): {
                return <span>Username to short (min 3)</span>
            }
            default: return <span>Error</span>
        }
    })();

    return (
        <>
            <form className='loginForm' onSubmit={handleSubmit(onSubmit)}>
                <input className='loginInput' id="user" type="text" placeholder="What's your name?" {...register("username", usernameConfig)} />
                <div className='buttonContainer'>
                    {loading ? <p>Logging in</p> : <button className='loginButton' type='submit'>Continue</button>}
                    {apiError && <p>{apiError}</p>}
                    {errorMessage}
                </div>
            </form>

        </>
    )
}


export default LoginForm